# MPR: Multi-Cue Photometric Registration

Check out the video here (click the image below):

[![](http://img.youtube.com/vi/56SFGLOk9fw/0.jpg)](http://www.youtube.com/watch?v=56SFGLOk9fw "MPR: Flexible Multi-Cue Photometric Point Cloud Registration")


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


## Installation

### Prerequisites

* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules) - _collection of cmake modules_
* [srrg_core](https://gitlab.com/srrg-software/srrg_core) - _core, i.e. low-level algorithms and utilities_

There is a couple of ways how to get these:

### Get dependencies automatically
The easiest way is to use `catkin deps fetch` command from package
[catkin_tools_fetch](https://github.com/Photogrammetry-Robotics-Bonn/catkin_tools_fetch):
```bash
sudo apt-get install python-pip                                            # install pip
sudo pip install catkin_tools_fetch                                        # install catkin_tools_fetch

cd catkin_ws                                                               # navigate to the catkin workspace
git clone https://gitlab.com/srrg-software/srrg_mpr src/srrg_mpr           # clone this repo
catkin deps fetch --default_url https://gitlab.com/srrg-software srrg_mpr  # fetch its dependencies
catkin build                                                               # build all packages
```

It will clone the required repos and build everything you need.


### [alternative] Get dependencies manually

Clone the following repos in your catkin workspace

```bash
cd catkin_ws/src  # navigate to the catkin workspace
git clone https://gitlab.com/srrg-software/srrg_cmake_modules
git clone https://gitlab.com/srrg-software/srrg_core
git clone https://gitlab.com/srrg-software/srrg_mpr
cd ..         # return to catkin workspace root
catkin build  # build all packages
```

## Run the code on test data

### Test 1 - RGB-D data

**1.** download the TUM Dataset sequence [_fr1/desk_](https://drive.google.com/file/d/0BxhfDMgREiwXc1pwY0lWVVU1LTQ/view?usp=sharing) (320 MB) into a folder on your computer

**2.** launch a terminal in the folder and uncompress the tarball

`tar -xzvf rgbd_dataset_freiburg1_desk.tar.gz`

**3.** run the example directly in the folder

`cd rgbd_dataset_freiburg1_desk`

`. run_srrg_mpr.sh`

The script will execute the `mpr_tracker_app` and will evaluate the result with the TUM Relative Pose Error computation script.

### Test 2 - RobotEye Laser data

**1.** download the S.Gennaro Dataset [_S-Gennaro/corridor_](https://drive.google.com/file/d/0BxhfDMgREiwXaWJyRGdGSHpnaFk/view?usp=sharing) (size 3 MB)

**2.** launch a terminal in the folder and uncompress the tarball

`tar -xzvf Naples-laser-big-church-and-tunnel.tar.gz`

**3.** run the example directly in the folder

`. run_srrg_mpr.sh`

### Test 3 - Velodyne H64 data

**1.** download the KITTI Dataset sequence 10 [_kitti/10_](https://drive.google.com/file/d/0BxhfDMgREiwXNUxNOFprb0d3MEE/view?usp=sharing) (size 85 MB)

**2.** launch a terminal in the folder and uncompress the tarball

`tar -xzvf KITTI_velodyne_10.tar.gz`

**3.** run the example script directly in the folder

`. run_srrg_mpr.sh`

The script will execute the `mpr_tracker_app` and will evaluate the result with the KITTI odometry evaluation utility.

## Authors

* Bartolomeo Della Corte
* Igor Bogoslavskyi
* Cyrill Stachniss
* Giorgio Grisetti

## License and credits

This project is licensed under the FreeBSD License - see the LICENSE file for details.

The logo adapted from [Dribbble](https://dribbble.com/shots/3372412-Falling-Lasagna).
