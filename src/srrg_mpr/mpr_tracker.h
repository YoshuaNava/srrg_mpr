#pragma once
#include "mpr_aligner.h"
#include "mpr_pyramid_generator.h"
#include "mpr_solver.h"
#include "mpr_stats.h"

namespace srrg_mpr {
using namespace std;

typedef std::vector<MPRSolver, Eigen::aligned_allocator<MPRSolver> >
    MPRSolverVector;

class MPRTracker : public MPRAligner {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  struct Config : public MPRAligner::Config {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    float keyframe_max_translation = 0.1;
    float keyframe_max_rotation = 0.1;
    bool verbose = false;
  };

  // this has to be called explicitly after construction
  virtual void setup() override;

  Config& mutableConfig();

  const Config& constConfig() const;

  virtual void setImages(const srrg_core::RawDepthImage& raw_depth,
                         const srrg_core::RGBImage& raw_rgb);

  void setGlobalT(const Eigen::Isometry3f& global_T) { _global_T = global_T; }
  const Eigen::Isometry3f& globalT() const { return _global_T; }

  void setRelativeMotionGuess(const Eigen::Isometry3f& relative_motion_guess) {
    _relative_motion_guess = relative_motion_guess;
  }

  virtual void compute() override;

 protected:
  Eigen::Isometry3f _global_T = Eigen::Isometry3f::Identity();

  MPRPyramid _keyframe;
  Eigen::Isometry3f _keyframe_T = Eigen::Isometry3f::Identity();

  Eigen::Isometry3f _relative_motion_guess = Eigen::Isometry3f::Identity();

  MPRPyramid _last_good_frame;
  Eigen::Isometry3f _last_good_frame_in_keyframe_T =
      Eigen::Isometry3f::Identity();

  Config* _config_ptr = nullptr;
};
}
